package ir.filimo.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import ir.filimo.home.databinding.ItemVideoBinding
import ir.filimo.model.CVideo

class VideoAdapter : RecyclerView.Adapter<VideoAdapter.VideoViewHolder> (){
    private val items = arrayListOf<CVideo>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VideoViewHolder =
        VideoViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_video, parent, false)
        )

    override fun onBindViewHolder(holder: VideoViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int =
        items.size

    fun updateAdapter(newItems: List<CVideo>) {
        items.clear()
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    inner class VideoViewHolder(private val parent: View): RecyclerView.ViewHolder(parent) {
        private val binding = ItemVideoBinding.bind(parent)
        fun bind(item: CVideo) {
            binding.video = item

            Glide.with(parent.context).load(item.picture).into(binding.ivThumbnail)
        }
    }
}