package ir.filimo.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import ir.filimo.common.AppDispatchers
import ir.filimo.common.base.BaseViewModel
import ir.filimo.model.CVideo
import ir.filimo.model.Resource
import ir.filimo.repository.VideoRepository
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val appDispatchers: AppDispatchers,
    private val videoRepository: VideoRepository
) : BaseViewModel() {

    private val _videos = MutableLiveData<Resource<List<CVideo>>>()
    val videos: LiveData<Resource<List<CVideo>>> get() = _videos

    fun searchVideo(search: String) = viewModelScope.launch(appDispatchers.main) {
        val dataSource: Resource<List<CVideo>>
        withContext(appDispatchers.io) {
            dataSource = videoRepository.searchInVideos(search)
        }
        _videos.postValue(dataSource)

        if (dataSource is Resource.Error) {
            onError(dataSource.error)
        }
    }
}