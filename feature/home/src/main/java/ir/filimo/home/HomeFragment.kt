package ir.filimo.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import ir.filimo.common.ConnectivityUtil
import ir.filimo.common.base.BaseFragment
import ir.filimo.common.base.BaseViewModel
import ir.filimo.common.extension.clear
import ir.filimo.common.extension.gone
import ir.filimo.common.extension.textToString
import ir.filimo.common.extension.visible
import ir.filimo.home.databinding.FragmentHomeBinding

@AndroidEntryPoint
class HomeFragment : BaseFragment() {
    private val viewModel: HomeViewModel by viewModels()
    private lateinit var binding: FragmentHomeBinding
    private val adapter = VideoAdapter()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        setupAdapter()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.edSearch.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                if (ConnectivityUtil.isNetworkConnected(requireContext())) {
                viewModel.searchVideo(binding.edSearch.textToString())
                showLoading()
                } else Toast.makeText(requireContext(), getString(R.string.no_internet), Toast.LENGTH_LONG).show()
            }
            false
        }
    }
    override fun getViewModel(): BaseViewModel =
        viewModel

    override fun setUpListeners() {
        super.setUpListeners()

        viewModel.videos.observe(viewLifecycleOwner) {
            hideSoftKeyboard()
            hideLoading()
            it.data?.let { videos ->
                if (videos.isNotEmpty()) {
                    binding.rcVideo.scheduleLayoutAnimation()
                    adapter.updateAdapter(videos)
                    binding.edSearch.clear()

                    binding.ivNoVideo.gone()
                    binding.tvTitle.gone()
                } else {
                    binding.ivNoVideo.visible()
                    binding.tvTitle.visible()
                }
            }
        }
    }
    private fun setupAdapter() {
        val animationController = AnimationUtils.loadLayoutAnimation(
            requireContext(),
            ir.filimo.common.R.anim.layout_animation
        )
        binding.rcVideo.layoutAnimation = animationController
        binding.rcVideo.scheduleLayoutAnimation()

        binding.rcVideo.adapter = adapter
    }

}