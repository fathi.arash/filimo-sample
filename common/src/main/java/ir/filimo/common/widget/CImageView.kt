package ir.filimo.common.widget

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatImageView
import ir.filimo.common.R

class CImageView : AppCompatImageView {
    private var radius: Int? = null
    private var circleColor: Int? = null

    constructor(context: Context) : super(context) {
        init(context, null)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(context, attrs)
    }

    @SuppressLint("CustomViewStyleable")
    private fun init(context: Context, attrs: AttributeSet?) {
        val a = context.obtainStyledAttributes(attrs, R.styleable.CImageView, 0, 0)
        radius = a.getInt(R.styleable.CImageView_c_radius, 0)
        circleColor = a.getColor(R.styleable.CImageView_c_circle_color, 0)
        a.recycle()
        attrHandler()
    }

    private fun attrHandler() {
        circleColorHandler()
    }

    private fun circleColorHandler() {
        if (circleColor == 0) return
        val gradientDrawable = GradientDrawable()
        gradientDrawable.cornerRadius = 140F
        circleColor?.let { gradientDrawable.setColor(it) }
        background = gradientDrawable
    }
}