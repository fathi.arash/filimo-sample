package ir.filimo.common.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import ir.filimo.common.AppDispatchers
import kotlinx.coroutines.Dispatchers

@InstallIn(ViewModelComponent::class)
@Module
object CommonModule {
    @Provides
    fun provideAppDispatcher() = AppDispatchers(Dispatchers.Main, Dispatchers.IO)

}