package ir.filimo.common.base

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.FragmentNavigator
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import ir.filimo.common.extension.setupSnackbar
import com.google.android.material.snackbar.Snackbar
import ir.filimo.common.ui.ProgressDialogFragment
import ir.filimo.navigation.NavigationCommand


abstract class BaseFragment : Fragment() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeNavigation(getViewModel())
        setUpListeners()
        setupSnackbar(this, getViewModel().snackBarError, Snackbar.LENGTH_LONG)
    }

    protected open fun setUpListeners() {}

    abstract fun getViewModel(): BaseViewModel

    private fun observeNavigation(viewModel: BaseViewModel) {
        viewModel.navigation.observe(viewLifecycleOwner) {
            it?.getContentIfNotHandled()?.let { command ->
                when (command) {
                    is NavigationCommand.To -> findNavController().navigate(
                        command.directions,
                        getExtras()
                    )
                    is NavigationCommand.Back -> findNavController().navigateUp()
                }
            }
        }
    }

    protected fun hideSoftKeyboard() {
        val imm =
            requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(requireView().windowToken, 0)
        requireActivity().window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
    }

    protected open fun showLoading() {
        val progressDialog = ProgressDialogFragment()
        progressDialog.isCancelable = false
        progressDialog.show(requireActivity().supportFragmentManager, "dialog")
    }

    protected fun hideLoading() {
        val fragment = requireActivity().supportFragmentManager.findFragmentByTag("dialog")
        fragment?.let {
            requireActivity().supportFragmentManager.beginTransaction().remove(it).commit()
        }
    }

    open fun getExtras(): FragmentNavigator.Extras = FragmentNavigatorExtras()

}