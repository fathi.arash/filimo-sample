package ir.filimo.remote.utils

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import ir.filimo.model.ErrorBody
import ir.filimo.model.ErrorModel
import retrofit2.HttpException
import java.io.IOException


interface ErrorManager {
    fun errorModel(error: Throwable): ErrorModel
    fun httpErrorModel(message: String, error: HttpException): ErrorModel
}

class RemoteErrorManager : ErrorManager {
    override fun errorModel(error: Throwable): ErrorModel {
        return when (error) {
            is HttpException -> {
                val s = error.response()?.errorBody()?.string()
                httpErrorModel(s!!, error)
            }
            is IOException -> ErrorModel(ErrorModel.Type.NETWORK, error.message)
            else -> ErrorModel(ErrorModel.Type.UNEXPECTED, error.message)
        }
    }

    override fun httpErrorModel(message: String, error: HttpException): ErrorModel {
        return try {
            val model = Gson().fromJson<ErrorBody>(
                message,
                object : TypeToken<ErrorBody>() {
                }.type
            )
            ErrorModel(
                ErrorModel.Type.HTTP,
                model?.message ?: error.message(),
            )
        } catch (e: Exception) {
            ErrorModel(ErrorModel.Type.HTTP, error.message())
        }
    }
}
