package ir.filimo.remote.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import ir.filimo.remote.BuildConfig
import ir.filimo.remote.retrofit.HttpInterceptor
import ir.filimo.remote.retrofit.buildRetrofit
import ir.filimo.remote.service.VideoService
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit
import javax.inject.Qualifier

@Module
@InstallIn(SingletonComponent::class)
object RemoteModule {

    @Provides
    fun provideInterceptor(): Interceptor =
        HttpInterceptor()

    @Qualifier
    @Retention(AnnotationRetention.BINARY)
    annotation class FilimoRetrofit

    @Provides
    fun provideOKHttpClient(interceptor: Interceptor): OkHttpClient =
        OkHttpClient.Builder()
            .readTimeout(15, TimeUnit.SECONDS)
            .addInterceptor(interceptor)
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .build()

    @FilimoRetrofit
    @Provides
    fun provideRetrofit(client: OkHttpClient): Retrofit =
        buildRetrofit(BuildConfig.FILIMO_BASE_URL, client)


    @Provides
    fun provideVehicleService(@FilimoRetrofit retrofit: Retrofit): VideoService =
        retrofit.create(VideoService::class.java)

}