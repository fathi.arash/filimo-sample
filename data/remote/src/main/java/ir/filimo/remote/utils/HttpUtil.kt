package ir.filimo.remote.utils

import android.util.Base64

fun basicAuthorizationHeader(clientId: String, clientSecret: String): String {
    val rawString = "$clientId:$clientSecret"
    return "basic " + Base64.encodeToString(rawString.toByteArray(), Base64.NO_WRAP)
}