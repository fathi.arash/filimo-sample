package ir.filimo.remote.retrofit

import okhttp3.Interceptor
import okhttp3.Response

class HttpInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()

        val request = original.newBuilder().apply {
            addHeader("jsonType", "simple")
        }.build()
        return chain.proceed(request)
    }
}