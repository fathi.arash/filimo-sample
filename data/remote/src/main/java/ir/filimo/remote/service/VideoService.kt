package ir.filimo.remote.service

import ir.filimo.remote.model.VideoSearchResponse
import kotlinx.coroutines.Deferred
import retrofit2.http.*


interface VideoService {
    @GET("api/en/v1/movie/movie/list/tagid/1000300/text/{query}/sug/on")
    fun searchVideoAsync(@Path("query") keyword: String): Deferred<VideoSearchResponse>
}