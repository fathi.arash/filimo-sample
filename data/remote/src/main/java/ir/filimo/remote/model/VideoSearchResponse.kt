package ir.filimo.remote.model

import com.google.gson.annotations.SerializedName

data class VideoSearchResponse(
    val data: List<Data>
)

data class Data(
    @SerializedName("movie_title")
    val title: String,

    @SerializedName("rate_avrage")
    val rateAvg: String,

    @SerializedName("imdb_rate")
    val imdbRate: String,
    val pic: Picture
    )

data class Picture(
    @SerializedName("movie_img_s")
    val pic: String
)