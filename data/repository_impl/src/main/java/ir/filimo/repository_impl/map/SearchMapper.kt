package ir.filimo.repository_impl.map

import ir.filimo.remote.model.VideoSearchResponse
import ir.filimo.model.CVideo
import ir.filimo.remote.model.Data

fun Data.toCVideo() =
    CVideo(
        title,
        pic.pic,
        rateAvg,
        imdbRate
    )

fun searchMapper(search: List<Data>): List<CVideo> {
    val videos = arrayListOf<CVideo>()

    for (item in search) {
        videos.add(item.toCVideo())
    }
    return videos
}


