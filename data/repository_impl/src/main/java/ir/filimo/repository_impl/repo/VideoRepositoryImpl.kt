package ir.filimo.repository_impl.repo

import ir.filimo.remote.model.VideoSearchResponse
import ir.filimo.model.CVideo
import ir.filimo.model.Resource
import ir.filimo.remote.service.VideoService
import ir.filimo.repository.VideoRepository
import ir.filimo.repository_impl.map.searchMapper
import ir.filimo.repository_impl.utils.RemoteBoundResource
import kotlinx.coroutines.Deferred
import javax.inject.Inject

class VideoRepositoryImpl @Inject constructor(
    private val vimeoService: VideoService
): VideoRepository {
    override suspend fun searchInVideos(search: String): Resource<List<CVideo>> {
        return object : RemoteBoundResource<List<CVideo>,VideoSearchResponse>() {
            override fun processResponse(response: VideoSearchResponse): List<CVideo> {
                return searchMapper(response.data)
            }

            override fun createCallAsync(): Deferred<VideoSearchResponse> =
                vimeoService.searchVideoAsync(search)

        }.build()
    }
}