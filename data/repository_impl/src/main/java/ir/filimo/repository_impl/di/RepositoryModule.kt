package ir.filimo.repository_impl.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import ir.filimo.remote.service.VideoService
import ir.filimo.repository.VideoRepository
import ir.filimo.repository_impl.repo.VideoRepositoryImpl

@Module
@InstallIn(ViewModelComponent::class)
object RepositoryModule {
    @Provides
    fun provideVideoRepository(
        vimeoService: VideoService
    ): VideoRepository =
        VideoRepositoryImpl(vimeoService)
}