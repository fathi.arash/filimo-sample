package ir.filimo.repository_impl.utils

import androidx.annotation.MainThread
import androidx.annotation.WorkerThread
import ir.filimo.model.ErrorModel
import ir.filimo.model.Resource
import ir.filimo.remote.utils.RemoteErrorManager
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.async
import retrofit2.HttpException
import kotlin.coroutines.coroutineContext

abstract class RemoteBoundResource<ResultType, RequestType> {

    private lateinit var result: Resource<ResultType>
    private val supervisorJob = SupervisorJob()

    suspend fun build(): Resource<ResultType> {
        return CoroutineScope(coroutineContext).async(supervisorJob) {
            try {
                baseProcessDatabase()
                val data = fetchFromNetwork()
                saveCallResults(data)
                return@async Resource.Success(data)
            } catch (e: HttpException) {
                return@async Resource.Error(
                    RemoteErrorManager().httpErrorModel(e.message(), e),
                    null
                )
            } catch (e: Exception) {
                e.printStackTrace()
                return@async Resource.Error(
                    ErrorModel(ErrorModel.Type.UNEXPECTED, "unexpected"),
                    null
                )
            }
        }.await()
    }

    private suspend fun fetchFromNetwork(): ResultType {
        val apiResponse = createCallAsync().await()
        return processResponse(apiResponse)
    }


    @WorkerThread
    protected abstract fun processResponse(response: RequestType): ResultType

    @WorkerThread
    protected open suspend fun saveCallResults(items: ResultType) {
    }

    @MainThread
    protected open suspend fun baseProcessDatabase() {
    }

    @MainThread
    protected abstract fun createCallAsync(): Deferred<RequestType>
}