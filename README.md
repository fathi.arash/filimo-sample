**saba idea filimo sample:**

For this project i use clean-mvvm arch and modules are:

**data:remote**
This module contains API call and retrofit classes

**data:repository_impl**
This module implemented the logic of repository

**domain:repository**
This module contains repository interfaces

**domain:model**
This module contains custom models that top level modules use

**app**
Android base module that contains MainActivity that use jet pack navigation

**features:home**
App base feature that you can search and see the result

**common**
Share modules that contains classes that can be share betwean all modules

**navigation**
Contains nav graph

Libraries:
- Retrofit
- Glide
- Navigation



you can sownload the apk : https://gitlab.com/fathi.arash/filimo-sample/-/tags/Release1
