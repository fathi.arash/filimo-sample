package ir.filimo.model

data class ErrorModel(
    val type: Type = Type.UNEXPECTED,
    val message: String? = null
) {

    enum class Type {
        HTTP,
        NETWORK,
        UNEXPECTED,
        LOCATION,
        EMPTY_DATA
    }
}

