package ir.filimo.model

data class CVideoDetails(
    val name: String,
    val duration: String,
    val description: String?,
    val url :String,
    val picture: String,
    val likes: Int,
    val comments: Int,
    val playedTime: Int
)
