package ir.filimo.model

data class CVideo(
    val name: String,
    val picture: String,
    val rate: String,
    val imdbRate: String
)