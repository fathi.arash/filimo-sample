package ir.filimo.model

sealed class Resource<out T>(open val data: T?) {
    data class Success<T>(override val data: T) : Resource<T>(data)
    data class Error<T>(val error: ErrorModel, override val data: T? = null) : Resource<T>(data)
    data class Loading<T>(override val data: T? = null) : Resource<T>(data)
}
