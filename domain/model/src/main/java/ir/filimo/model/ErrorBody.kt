package ir.filimo.model

data class ErrorBody(
    val message: String?
)
