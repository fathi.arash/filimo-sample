package ir.filimo.repository

import ir.filimo.model.CVideo
import ir.filimo.model.CVideoDetails
import ir.filimo.model.Resource

interface VideoRepository {
    suspend fun searchInVideos(search: String): Resource<List<CVideo>>
}